package ru.nirinarkhova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class ProjectChangeStatusByNameCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-change-status-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "change project status by project name.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PROJECT]");
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER PROJECT STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusId);
        @NotNull final Optional<Project> project = serviceLocator.getProjectService().findByName(userId, name);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        @NotNull final Optional<Project> projectStatusUpdate = serviceLocator.getProjectService().changeStatusByName(userId, name, status);
        Optional.ofNullable(projectStatusUpdate).orElseThrow(ProjectNotFoundException::new);
    }

}

