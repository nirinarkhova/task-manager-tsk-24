package ru.nirinarkhova.tm.api.entity;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

public interface IWBS extends  IHasCreated, IHasName, IHasDateStart, IHasStatus{

    @Override
    default void setCreated(@NotNull Date date) {

    }

}
