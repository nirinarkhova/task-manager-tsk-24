package ru.nirinarkhova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.api.repository.IUserRepository;
import ru.nirinarkhova.tm.api.service.IUserService;
import ru.nirinarkhova.tm.enumerated.Role;
import ru.nirinarkhova.tm.exception.empty.*;
import ru.nirinarkhova.tm.exception.entity.UserNotFoundException;
import ru.nirinarkhova.tm.model.User;
import ru.nirinarkhova.tm.util.HashUtil;

import java.util.Optional;

public class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = new User();
        user.setLogin(login);
        user.setRole(Role.USER);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Nullable
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    public Optional<User> setPassword(@Nullable final String userId,@Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final Optional<User> user = findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(password);
        user.ifPresent(u -> u.setPasswordHash(hash));
        return user;
    }

    @Nullable
    @Override
    public Optional<User> findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    public Optional<User> findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return findByEmail(email) != null;
    }

    @Nullable
    @Override
    public Optional<User> updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Optional<User> user = findById(userId);
        user.ifPresent(u -> {
            u.setFirstName(firstName);
            u.setLastName(lastName);
            u.setMiddleName(middleName);
        });

        return user;
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    @Override
    public void lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final Optional<User> user = userRepository.findByLogin(login);
        user.ifPresent(u -> u.setLocked(true));
        user.orElseThrow(UserNotFoundException::new);
    }

    @Override
    public void unlockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final Optional<User> user = userRepository.findByLogin(login);
        user.ifPresent(u -> u.setLocked(false));
        user.orElseThrow(UserNotFoundException::new);
    }

}
